<?php
/**
 * Theme footer.
 *
 * @package march/theme
 */

?>
<footer>
	<div class="container-xl">
		<div class="row">
			<div class="col-12">
				<?php
				if ( has_nav_menu( 'top_bar_menu' ) ) {
					wp_nav_menu(
						[
							'theme_location' => 'top_bar_menu',
							'container'      => '',
							'menu_class'     => 'soc',
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
				}
				?>
				<p class="copyright">
					<?php echo wp_kses_post( carbon_get_theme_option( 'ma_copyright' ) ); ?>
				</p>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
