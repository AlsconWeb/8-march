/* global maObject */

/**
 * @param maObject.ajaxUrl
 * @param maObject.nonce
 * @param maObject.actionName
 * @param maObject.countUpAction
 * @param maObject.nonceCountUp
 */

jQuery( document ).ready( function( $ ) {
	let couponID = 0;

	$( '.items a' ).click( function( e ) {
		e.preventDefault();

		couponID = $( this ).data( 'coupon' );
		let element = $( this );

		let data = {
			action: maObject.actionName,
			couponID: couponID,
			nonce: maObject.nonce
		}

		$.ajax( {
			type: 'POST',
			url: maObject.ajaxUrl,
			data: data,
			success: function( res ) {

				if ( res.success ) {
					let modal = $( '.product' );

					$( '.ajax-content' ).remove();
					modal.append( res.data.html );

					element.parents( '.items' ).addClass( 'hide' );
					modal.addClass( 'open' );

					$( '.product .icon-close' ).click( function( e ) {
						e.preventDefault();
						modal.removeClass( 'open' );
						$( '.items' ).removeClass( 'hide' );
					} );

					sendCounter( couponID );
				}

			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );


	} );

	function sendCounter( couponID ) {
		$( '.link-up' ).click( function( e ) {
			let data = {
				action: maObject.countUpAction,
				couponID: couponID,
				nonce: maObject.nonceCountUp
			}

			$.ajax({
			    type: 'POST',
			    url: maObject.ajaxUrl,
			    data: data,
			    success: function(data) {
			        
			    },
			    error:function (xhr, ajaxOptions, thrownError){
			        console.log('error...', xhr);
			        //error logging
			    },
			});
		} );
	}

	$( 'a[href*="#"]:not([href="#"])' ).click( function() {
		if ( location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' ) && location.hostname == this.hostname ) {
			let target = $( this.hash );
			target = target.length ? target : $( '[name=' + this.hash.slice( 1 ) + ']' );

			if ( target.length ) {
				$( 'html,body' ).animate( {
					scrollTop: target.offset().top - 100
				}, 1000 );
				return false;
			}

		}
	} );


} );