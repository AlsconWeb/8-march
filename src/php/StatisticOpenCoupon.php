<?php
/**
 * Statistic open coupon widget.
 *
 * @package march/theme
 */

namespace March\Theme;

/**
 * StatisticOpenCoupon class file.
 */
class StatisticOpenCoupon {
	/**
	 * StatisticOpenCoupon construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_dashboard_setup', [ $this, 'register_widget' ] );
	}

	/**
	 * Show widget
	 *
	 * @return void
	 */
	public function register_widget(): void {
		wp_add_dashboard_widget(
			'ma_statistic_coupon',
			__( 'Statistic open coupon', 'march' ),
			[ $this, 'dashboard_widget_statistic_handler' ],
			[ $this, 'dashboard_widget_statistic_config_handler' ]
		);
	}

	/**
	 * Statistic handler.
	 *
	 * @return void
	 */
	public function dashboard_widget_statistic_handler(): void {
		$arg = [
			'post_type'      => 'coupon',
			'posts_per_page' => - 1,
			'post_status'    => 'publish',
			'order'          => 'ASC',
		];

		$coupon_query = new \WP_Query( $arg );

		if ( $coupon_query->have_posts() ) {
			?>
			<ul>
				<?php
				while ( $coupon_query->have_posts() ) {
					$coupon_query->the_post();

					$count = get_post_meta( get_the_ID(), 'ma_coupon_view', true );
					?>
					<li style="display: flex;flex-direction: row;justify-content: space-between; margin-bottom: 10px;">
						<p style="margin: 0;"><?php the_title(); ?></p>
						<span><b><?php echo esc_html( $count ?: 0 ); ?></b></span>
					</li>
					<?php
				}
				wp_reset_postdata();
				?>
			</ul>
			<?php
		}
	}

	public function dashboard_widget_statistic_config_handler(): void {

	}

}
