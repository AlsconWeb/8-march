<?php
/**
 * Moin theme class.
 *
 * @package march/theme
 */

namespace March\Theme;

/**
 * Main class file.
 */
class Main {

	/**
	 * Theme version.
	 */
	const MA_VERSION = '1.0.0';

	/**
	 * Modal nonce and action.
	 */
	const MA_MODAL_ACTION = 'ma_coupon_modal';

	/**
	 * Count up action.
	 */
	const MA_COUNT_UP_ACTION = 'ma_count_up_modal';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );

		// ajax.
		add_action( 'wp_ajax_' . self::MA_MODAL_ACTION, [ $this, 'load_modal_content' ] );
		add_action( 'wp_ajax_nopriv_' . self::MA_MODAL_ACTION, [ $this, 'load_modal_content' ] );

		add_action( 'wp_ajax_' . self::MA_COUNT_UP_ACTION, [ $this, 'count_up' ] );
		add_action( 'wp_ajax_nopriv_' . self::MA_COUNT_UP_ACTION, [ $this, 'count_up' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );

		new CPT();
		new CarbonFields();
		new StatisticOpenCoupon();
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script( 'ma_build', $url . '/assets/js/build' . $min . '.js', [ 'jquery' ], self::MA_VERSION, true );

		wp_localize_script(
			'ma_build',
			'maObject',
			[
				'ajaxUrl'       => admin_url( 'admin-ajax.php' ),
				'nonce'         => wp_create_nonce( self::MA_MODAL_ACTION ),
				'actionName'    => self::MA_MODAL_ACTION,
				'countUpAction' => self::MA_COUNT_UP_ACTION,
				'nonceCountUp'  => wp_create_nonce( self::MA_COUNT_UP_ACTION ),

			]
		);

		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-font', '//fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&amp;display=swap', '', '1.0' );
		wp_enqueue_style( 'ma_main', $url . '/assets/css/main' . $min . '.css', '', self::MA_VERSION );
		wp_enqueue_style( 'ma_style', $url . '/style.css', '', self::MA_VERSION );

	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo' );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu' => __( 'Social network', 'march' ),
			]
		);

		// post thumbnail.
		add_theme_support( 'post-thumbnails' );

		// add images sizes.
		add_image_size( 'ma-coupon-image', 580, 760, [ 'top', 'center' ] );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {

		$home  = esc_url( get_bloginfo( 'url' ) );
		$class = 'logo';
		if ( has_custom_logo() ) {
			$white_logo = get_theme_mod( 'ma_logo' );
			$logo       = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'logo-img',
					'itemprop' => 'logo',
				]
			);
			$content    = $logo;

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				$home,
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Ajax handler load modal content.
	 *
	 * @return void
	 */
	public function load_modal_content(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : '';

		if ( ! wp_verify_nonce( $nonce, self::MA_MODAL_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'march' ) ] );
		}

		$coupon_id = ! empty( $_POST['couponID'] ) ? filter_var( wp_unslash( $_POST['couponID'] ), FILTER_VALIDATE_INT ) : null;

		if ( empty( $coupon_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Передан не верный Купон ID', 'march' ) ] );
		}

		$coupon_type = carbon_get_post_meta( $coupon_id, 'ma_coupon_style' );

		ob_start();

		if ( 'link' === $coupon_type ) {
			get_template_part( 'template-part/modal', 'content', [ 'coupon_id' => $coupon_id ] );
		} else {
			get_template_part( 'template-part/modal', 'press', [ 'coupon_id' => $coupon_id ] );
		}

		wp_send_json_success( [ 'html' => ob_get_clean() ] );
	}

	/**
	 * Ajax handler count up.
	 *
	 * @return void
	 */
	public function count_up(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : '';

		if ( ! wp_verify_nonce( $nonce, self::MA_COUNT_UP_ACTION ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'march' ) ] );
		}

		$coupon_id = ! empty( $_POST['couponID'] ) ? filter_var( wp_unslash( $_POST['couponID'] ), FILTER_VALIDATE_INT ) : null;

		$count = get_post_meta( $coupon_id, 'ma_coupon_view', true );
		if ( ! $count ) {
			$count = 0;
		}

		$count ++;

		update_post_meta( $coupon_id, 'ma_coupon_view', $count );

		wp_send_json_success();
	}
}
