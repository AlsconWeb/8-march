<?php
/**
 * Create custom post type.
 *
 * @package iwpdev/alevel
 */

namespace March\Theme;

/**
 * CPT class file.
 */
class CPT {

	/**
	 * CPT construct.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register_coupon' ] );
	}

	/**
	 * Register custom post type coupon.
	 *
	 * @return void
	 */
	public function register_coupon(): void {
		register_post_type(
			'coupon',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Coupons', 'march' ),
					'singular_name'      => __( 'Coupon', 'march' ),
					'add_new'            => __( 'Add coupon', 'march' ),
					'add_new_item'       => __( 'Adding a coupon', 'march' ),
					'edit_item'          => __( 'Editing coupon', 'march' ),
					'new_item'           => __( 'New coupon', 'march' ),
					'view_item'          => __( 'View coupon', 'march' ),
					'search_items'       => __( 'Search coupon', 'march' ),
					'not_found'          => __( 'Not found', 'march' ),
					'not_found_in_trash' => __( 'Not found in trash', 'march' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Coupons', 'alevel' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 5,
				'menu_icon'     => 'dashicons-tickets',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
				],
				'taxonomies'    => [],
				'has_archive'   => false,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}
}
