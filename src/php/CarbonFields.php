<?php
/**
 * Carbone Fields init class.
 *
 * @package march/theme
 */

namespace March\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields file.
 */
class CarbonFields {
	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'load_carbon_fields' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'register_coupon_fields' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'register_theme_options' ] );
	}

	/**
	 * Load main class Carbon Field.
	 *
	 * @return void
	 */
	public function load_carbon_fields(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Register coupon fields.
	 *
	 * @return void
	 */
	public function register_coupon_fields(): void {
		Container::make( 'post_meta', __( 'Coupon text', 'march' ) )
			->where( 'post_type', '=', 'coupon' )
			->add_fields(
				[
					Field::make( 'radio', 'ma_coupon_style', __( 'Coupon style', 'march' ) )
						->add_options(
							[
								'link'  => __( 'Link type', 'march' ),
								'press' => __( 'Press', 'march' ),
							]
						),
					Field::make( 'text', 'ma_coupon_button_text', __( 'Coupon button text', 'march' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field' => 'ma_coupon_style',
									'value' => 'link',
								],
							]
						),
					Field::make( 'text', 'ma_coupon_button_link', __( 'Coupon button link', 'march' ) )
						->set_conditional_logic(
							[
								'relation' => 'AND',
								[
									'field' => 'ma_coupon_style',
									'value' => 'link',
								],
							]
						),
				]
			);
	}

	/**
	 * Theme options.
	 *
	 * @return void
	 */
	public function register_theme_options(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Basic Options', 'march' ) )
			->add_fields(
				[
					Field::make( 'header_scripts', 'ma_header_script', __( 'Header Script', 'march' ) ),
					Field::make( 'footer_scripts', 'ma_footer_script', __( 'Footer Script', 'march' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Footer', 'march' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'rich_text', 'ma_copyright', __( 'Copyright', 'march' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Main page', 'march' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'ma_title', __( 'Title', 'march' ) ),
					Field::make( 'text', 'ma_text', __( 'Text', 'march' ) ),
					Field::make( 'text', 'ma_text_button', __( 'Text button', 'march' ) )->set_width( 50 ),
					Field::make( 'image', 'ma_main_image', __( 'Main image', 'march' ) )->set_width( 50 ),
				]
			);
	}
}
