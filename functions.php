<?php
/**
 * Functions Theme file.
 *
 * @package march/theme
 */


use March\Theme\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();
