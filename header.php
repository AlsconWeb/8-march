<?php
/**
 * Theme header.
 *
 * @package march/theme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
	<div class="container-xl">
		<div class="row justify-content-between align-items-center">
			<div class="col-auto">
				<?php the_custom_logo(); ?>
			</div>
			<div class="col-auto">
				<?php
				if ( has_nav_menu( 'top_bar_menu' ) ) {
					wp_nav_menu(
						[
							'theme_location' => 'top_bar_menu',
							'container'      => '',
							'menu_class'     => 'soc',
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
				}
				?>
			</div>
		</div>
	</div>
</header>
