<?php
/**
 * Index theme file.
 *
 * @package march/theme
 */

$arg_coupon = [
	'post_type'      => 'coupon',
	'posts_per_page' => - 1,
	'post_status'    => 'publish',
	'orderby'        => 'rand',
];

$query_coupon = new WP_Query( $arg_coupon );

get_header();
?>
	<section>
		<div class="container-xl">
			<div class="row align-items-center">
				<div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<h1><?php echo esc_html( carbon_get_theme_option( 'ma_title' ) ); ?></h1>
					<p><?php echo esc_html( carbon_get_theme_option( 'ma_text' ) ); ?></p>
					<a class="button" href="#item-code">
						<?php echo esc_html( carbon_get_theme_option( 'ma_text_button' ) ); ?>
					</a>
				</div>
				<div class="col-xl-7 col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<?php
					$image_url = wp_get_attachment_image_url( carbon_get_theme_option( 'ma_main_image' ), 'full' );

					if ( empty( $image_url ) ) {
						$image_url = get_stylesheet_directory_uri() . '/assets/img/march-8th.png';
					}


					?>
					<img
							src="<?php echo esc_url( $image_url ); ?>"
							alt="Image 8-March">
				</div>
			</div>
			<div class="row" id="item-code">
				<div class="col-12">
					<?php if ( $query_coupon->have_posts() ) { ?>
						<div class="items">
							<?php
							while ( $query_coupon->have_posts() ) {
								$query_coupon->the_post();

								$coupon_id = get_the_ID();
								?>
								<div class="item">
									<a href="#" data-coupon="<?php echo esc_attr( $coupon_id ); ?>">
										<i class="icon-gift"></i>
									</a>
								</div>
								<?php
							}
							wp_reset_postdata();
							?>
						</div>
					<?php } ?>
					<div class="product">
						<div class="ajax-content">

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
