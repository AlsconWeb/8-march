<?php
/**
 * Modal type link content.
 *
 * @package march/theme
 */

$coupon_id = $args['coupon_id'];
$image_url = '';

if ( has_post_thumbnail( $coupon_id ) ) {
	$image_id  = get_post_thumbnail_id( $coupon_id );
	$image_url = wp_get_attachment_image_url( $image_id, 'full' );
} else {
	$image_url = 'https://via.placeholder.com/580x760';
}

?>
<div class="ajax-content">
	<div class="left-block">
		<img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( get_the_title( $coupon_id ) ); ?>">
	</div>
	<div class="right-block">
		<a class="icon-close" href="#"></a>
		<h2><?php echo wp_kses_post( get_the_title( $coupon_id ) ); ?></h2>
		<?php
		$content = get_the_content( null, null, $coupon_id );
		if ( ! empty( $content ) ) {
			echo '<p>' . wp_kses_post( $content ) . '</p>';
		}
		?>
		<div class="promo-code">
			<a
					href="<?php echo esc_url( carbon_get_post_meta( $coupon_id, 'ma_coupon_button_link' ) ); ?>"
					target="_blank"
					rel="noreferrer nofollow"
					class="link-up">
				<?php echo esc_html( carbon_get_post_meta( $coupon_id, 'ma_coupon_button_text' ) ); ?>
			</a>
		</div>
	</div>
</div>
