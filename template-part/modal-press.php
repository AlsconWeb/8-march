<?php
/**
 * Modal type press content.
 *
 * @package march/theme
 */

$coupon_id = $args['coupon_id'];
$image_url = '';

if ( has_post_thumbnail( $coupon_id ) ) {
	$image_id  = get_post_thumbnail_id( $coupon_id );
	$image_url = wp_get_attachment_image_url( $image_id, 'full' );
} else {
	$image_url = 'https://via.placeholder.com/580x760';
}

$coupon_type = carbon_get_post_meta( $coupon_id, 'ma_coupon_style' );

?>
<div class="ajax-content <?php echo esc_attr( $coupon_type ); ?>">
	<div class="left-block">
		<img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_html( get_the_title( $coupon_id ) ); ?>">
	</div>
	<div class="right-block">
		<a class="icon-close" href="#"></a>
		<h2><?php echo wp_kses_post( get_the_title( $coupon_id ) ); ?></h2>
	</div>
</div>
